
/* -------------------------------------
		  		ANIMATED TEXT
------------------------------------- */

 $('#js-rotating').Morphext({
  // The [in] animation type. Refer to Animate.css for a list of available animations.
  animation: 'bounceIn',
  // An array of phrases to rotate are created based on this separator. Change it if you wish to separate the phrases differently (e.g. So Simple | Very Doge | Much Wow | Such Cool).
	//separator: ',',
	separator: '|',
	// The delay between the changing of each phrase in milliseconds.
	speed: 3000,
	complete: function () {
	// Called after the entrance animation is executed.
	}
	});


$(function () {
    var $els = $('div[id^=rotate-header]'),
        i = 0,
        len = $els.length;

    // $els.slice(1).hide();
    $els.hide();
    $els.eq(0).fadeIn();
    setInterval(function () {
        $els.eq(i).fadeOut(function () {
            i = (i + 1) % len;
            $els.eq(i).fadeIn();
        });
    }, 7000);
});