//Header
$(function(){
  $('.header').load("header.html");
});

//Footer
$(function(){
  $('.footer').load("footer.html");
});

// Toggle tranparent navbar when the user scrolls the page

  $(window).scroll(function() {
    if($(this).scrollTop() > 50)  /*height in pixels when the navbar becomes non opaque*/ 
    {
        $('.opaque-navbar').addClass('nonopaque');
    } else {
        $('.opaque-navbar').removeClass('nonopaque');
    }
});

/* Scroll back to top */

$(document).ready(function() {
    var offset = 220;
    var duration = 500;
    $(window).scroll(function() {
        if ($(this).scrollTop() > offset) {
            $('.back-to-top').fadeIn(duration);
        } else {
            $('.back-to-top').fadeOut(duration);
        }
    });
    
    $('.back-to-top').click(function(event) {
        event.preventDefault();
        $('html, body').animate({scrollTop: 0}, duration);
        return false;
    });
});


/* -------------------------------------
		  		ANIMATED SPERM SHOES
		    	------------------------------------- */
			    $('#slideshow > div:gt(0)').hide();

			      setInterval(function() {
			        $('#slideshow > div:first')
			          .fadeOut(1000)
			          .next()
			          .fadeIn(1000)
			          .end()
			          .appendTo('#slideshow');
			      }, 7000);



/* ------------------------------------------
      Smoke
-------------------------------------------
    function smokeScreen(){
      var canvas = document.getElementById('canvas');
        var ctx = canvas.getContext('2d');
        canvas.width = innerWidth;
        canvas.height = innerHeight;

        var party = smokemachine(ctx, [160, 160, 2]);
        party.start(); // start animating

        onmousemove = function (e) {
          var x = e.clientX;
          var y = e.clientY;
          var n = 0.5;
          var t = Math.floor(Math.random() * 200) + 3800;
          party.addsmoke(x, y, n, t);
        };

        setInterval(function(){party.addsmoke(innerWidth/2, innerHeight, 1);}, 100);
    }*/
/*---------------- Height Match ---------------- */
// $(".matchHeight").matchHeight();
// $('.matchHeight').matchHeight({ property: 'min-height' });
// $(function() {
//   $('.matchHeight').matchHeight({
//         target: $('.shoesec')
//     });
// });

$('.matchHeight').each(function() {
  $(this).children('.leftHeight,.rightHeight').matchHeight({
    target: $(this).find('.rightHeight'),
    byRow: true,
    property: 'height',
    target: null,
    remove: false
  });
});

// toggle lets play


// animation effect



// 
$('a[href^="#aboutYsneakerParaSection"]').on('click', function(event) {

    var target = $(this.getAttribute('href'));

    if( target.length ) {
        event.preventDefault();
        $('html, body').stop().animate({
            scrollTop: target.offset().top
        }, 1000);
    }

});

$('a[href^="#sasvar"]').on('click', function(event) {

    var target = $(this.getAttribute('href'));

    if( target.length ) {
        event.preventDefault();
        $('html, body').stop().animate({
            scrollTop: target.offset().top
        }, 1000);
    }

});



/* Set the width of the side navigation to 250px */
function openNav() {
    document.getElementById("mySidenav").style.width = "250px";
}

/* Set the width of the side navigation to 0 */
function closeNav() {
    document.getElementById("mySidenav").style.width = "0";
}




// filteration inspiration

var $btns = $('.filter').click(function() {
  if (this.id == 'all') {
    $('#parentFilter > div').fadeIn(450);
  } else {
    var $el = $('.' + this.id).fadeIn(450);
    $('#parentFilter > div').not($el).hide();
  }
  $btns.removeClass('active');
  $(this).addClass('active');
});

// pop up for image view

$(document).ready(function() {

  $('.image-popup-vertical-fit').magnificPopup({
    type: 'image',
    closeOnContentClick: true,
    mainClass: 'mfp-img-mobile',
    image: {
      verticalFit: true
    }
    
  });  

});



// start filtering

$(document).ready(function() {
  $('#filterOptions li a').click(function() {
    // fetch the class of the clicked item
    var ourClass = $(this).attr('class');
    
    // reset the active class on all the buttons
    $('#filterOptions li').removeClass('active');
    // update the active state on our clicked button
    $(this).parent().addClass('active');
    
    if(ourClass == 'all') {
      // show all our items
      $('#ourHolder').children('div.item').show();  
    }
    else {
      // hide all elements that don't share ourClass
      $('#ourHolder').children('div:not(.' + ourClass + ')').hide();
      // show all elements that do share ourClass
      $('#ourHolder').children('div.' + ourClass).show();
    }
    return false;
  });
});

// end filtering


// $(document).ready(function(){
//     $(".slideMenuProduct").click(function(){
//         $(".sub-menu").slideToggle("slow");
//     });
// });

$(document).on('click', '.slideMenuProduct', function(){
  
  $(".sub-menu").slideToggle("slow");

  if($( "#menuSecondarySlider" ).hasClass( "menuExpanded" )){
    $(".menuSecondary").removeClass("menuExpanded");
  }else{
    $(".menuSecondary").addClass("menuExpanded");
  }
  
});







// product slider view
// $('.shoeSliderView').slick({
//    dots: false,
//    arrows:true,
//   infinite: true,
//   speed: 100,
//   fade: true,
//   cssEase: 'linear',
// });

// $('.aboutShoeView').slick({
//   autoplay: true,
//   autoplaySpeed: 200,
//   dots: false,
//   arrows:false,
//   infinite: true,  
//   fade: true,
// });


// animation on scroll initialization
AOS.init({
  duration:1200,

});



  
