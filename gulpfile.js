// Include gulp
var gulp = require('gulp');
 // Define base folders
var src = 'app/';
var dest = 'app/dist';
 // Include plugins
var concat = require('gulp-concat');
var uglify = require('gulp-uglify');
var rename = require('gulp-rename');
var cleanCSS = require('gulp-clean-css');
var sourcemaps = require('gulp-sourcemaps');
var order = require("gulp-order");
var htmlminify = require("gulp-html-minify");

//var sass = require('gulp-ruby-sass');
//var imagemin = require('gulp-imagemin');
var cache = require('gulp-cache');
 // Concatenate & Minify JS
gulp.task('scripts', function() {
    return gulp.src(src + ['js/!(packery-mode.pkgd.min)*.js'])
      //.pipe(concat('main.js'))
        .pipe(rename({suffix: '.min'}))
        .pipe(uglify())
        .pipe(gulp.dest(dest));
});
 // Compile CSS from Sass files
// gulp.task('sass', function() {
//     return sass('src/scss/style.scss', {style: 'compressed'})
//         .pipe(rename({suffix: '.min'}))
//         .pipe(gulp.dest('build/css'));
// });

// minify css
gulp.task('minify-css',function(){  
    return gulp.src(src + ['css/!(iconfonts)*.css'])
    .pipe(order([   
    "css/style.css",
    "css/responsive.css",
    "css/menu.css"
  
  ]))   

    .pipe(concat('concat_main.css'))
    .pipe(rename({suffix: '.min'}))
    // .pipe(sourcemaps.init())
    .pipe(cleanCSS())
    //.pipe(sourcemaps.write())
    .pipe(gulp.dest(dest));

}); 

gulp.task('bower-css',function(){
  var cssfiles =  ['bower_components/magnific-popup/dist/magnific-popup.css','bower_components/bootstrap/dist/css/bootstrap.css','bower_components/Morphext/dist/morphext.css'];

  return gulp.src(cssfiles)   
    
    .pipe(rename({suffix: '.min'}))
    // .pipe(sourcemaps.init())
    .pipe(cleanCSS())
    //.pipe(sourcemaps.write())
    .pipe(gulp.dest(dest));

}); 
 //minify css end


 //start html minify


// gulp.task('minify-html' , function(){
//     return gulp.src("*.html")
//         .pipe(htmlminify())
//         .pipe(gulp.dest("./"))
// });




//  gulp.task('images', function() {
//   return gulp.src(src + 'images/**/*')
//     .pipe(cache(imagemin({ optimizationLevel: 5, progressive: true, interlaced: true })))
//     .pipe(gulp.dest(dest + 'img'));
// });
 // Watch for changes in files
gulp.task('watch', function() {
   // Watch .js files
  gulp.watch(src + 'js/*.js', ['scripts']);
   // Watch .scss files
  gulp.watch(src + 'css/*.css', ['minify-css']);

  gulp.watch(cssfiles, ['bower-css']);
   // Watch image files
  // gulp.watch(src + 'images/**/*', ['images']);
 });
 // Default Task
gulp.task('default', ['scripts', 'minify-css', 'bower-css',  'watch']);

// 'minify-html',